package uk.co.bossymom.bossymom.DataTools;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.util.Pair;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessActivities;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static java.text.DateFormat.getDateTimeInstance;

/**
 * Data Handler for querying the Google Fit API.
 */
public class FitDataHandler {

    private FragmentActivity fragmentActivity;
    private Context context;
    public static GoogleApiClient gApiClient = null;

    private static final String TAG = "BossyMomFitDataHandler";

    boolean taskConsumed = false;
    boolean backgroundRequest;

    public FitDataHandler (FragmentActivity fragmentActivity) {
        this.fragmentActivity = fragmentActivity;
        this.context = fragmentActivity;
        backgroundRequest = false;
    }

    public FitDataHandler (Context context) {
        this.context = context;
        backgroundRequest = true;
    }

    /**
     *  Build the Fitness API client and run the apiTask provided.
     * @param apiTask represents the query task which can be built using generateQueryTask.
     */
    public void buildFitnessClient (final AsyncTask<Void, Void, ArrayList<DataReadResult>> apiTask) {
        // Create the Google API Client and register the scopes of the connection.
        GoogleApiClient.Builder builder = new GoogleApiClient.Builder(context);
        builder .addApi(Fitness.HISTORY_API)
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ))
                .addScope(new Scope(Scopes.FITNESS_NUTRITION_READ))
                .addScope(new Scope(Scopes.FITNESS_BODY_READ))
                .addConnectionCallbacks(
                        new GoogleApiClient.ConnectionCallbacks() {
                            @Override
                            public void onConnected(Bundle bundle) {
                                Log.i(TAG, "Connected to Google Fit API!");
                                //TODO Needs better fix to ensure onConnected is not called after resuming.gi
                                if (!taskConsumed) {
                                    apiTask.execute();
                                    taskConsumed = true;
                                }
                                else Log.e(TAG, "Attempting to run an already consumed task. Please create and build a new handler.");
                            }

                            @Override
                            public void onConnectionSuspended(int i) {
                                // If the connection to the server gets lost at some point,
                                // determine the reason and react to it.
                                if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_NETWORK_LOST) {
                                    Log.i(TAG, "Connection lost.  Cause: Network Lost.");
                                } else if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_SERVICE_DISCONNECTED) {
                                    Log.i(TAG, "Connection lost.  Reason: Service Disconnected");
                                }
                            }
                        });
        if (!backgroundRequest)
            builder.enableAutoManage(fragmentActivity, 0, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult result) {
                        Log.i(TAG, "Google Play services connection failed. Cause: " +
                                result.toString());
                    }
                });

        gApiClient = builder.build();
    }

    /**
     *  Build an AsyncTask to query the Fit API for the requested data.
     *  The requests are served in order, so the results are expected to be
     *  delivered in the same order as the requests.
     */
    public static AsyncTask<Void, Void, ArrayList<DataReadResult>> generateQueryTask(final FitDataRequester dataRequester,
                                                                                       final DataReadRequest... readRequests) {
        return new AsyncTask<Void, Void, ArrayList<DataReadResult>>() {
            ArrayList<DataReadResult> readResults = new ArrayList<>();

            @Override
            protected ArrayList<DataReadResult> doInBackground(Void... params) {
                for (DataReadRequest readRequest: readRequests) {
                    // Invoke the History API to fetch the data with the query and await the result of
                    // the read request.
                    DataReadResult dataReadResult =
                            Fitness.HistoryApi.readData(gApiClient, readRequest).await(1, TimeUnit.MINUTES);

                    readResults.add(dataReadResult);
                }

                return readResults;
            }

            @Override
            protected void onPostExecute(ArrayList<DataReadResult> dataReadResults) {
                dataRequester.onDataFetched(dataReadResults);
            }
        };
    }

    /**
     * Method to request the amount of expended calories in the last {@param nDays}.
     * Return a {@link DataReadRequest} for the calories expended.
     */
    public static DataReadRequest queryExpendedCalories(int nDays) {
        Pair<Long, Long> startEndTimes = startEndTimes(nDays);

        return new DataReadRequest.Builder()
                .aggregate(DataType.TYPE_CALORIES_EXPENDED, DataType.AGGREGATE_CALORIES_EXPENDED)
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(startEndTimes.first, startEndTimes.second, TimeUnit.MILLISECONDS)
                .build();
    }

    /**
     * Method to request amount of expended calories in the last day
     * with quarter-of-hour granularity.
     * Return a {@link DataReadRequest} for the active segments.
     */
    public static DataReadRequest queryExpendedCalories() {
        Pair<Long, Long> startEndTimes = startEndTimes(1);

        return new DataReadRequest.Builder()
                .aggregate(DataType.TYPE_CALORIES_EXPENDED, DataType.AGGREGATE_CALORIES_EXPENDED)
                .bucketByTime(15, TimeUnit.MINUTES)
                .setTimeRange(startEndTimes.first, startEndTimes.second, TimeUnit.MILLISECONDS)
                .build();
    }

    /**
     * Method to request activity data in the last {@param nDays}.
     * Return a {@link DataReadRequest} for the active segments.
     */
    public static DataReadRequest queryActiveTime(int nDays) {
        Pair<Long, Long> startEndTimes = startEndTimes(nDays);

        return new DataReadRequest.Builder()
                .aggregate(DataType.TYPE_ACTIVITY_SEGMENT, DataType.AGGREGATE_ACTIVITY_SUMMARY)
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(startEndTimes.first, startEndTimes.second, TimeUnit.MILLISECONDS)
                .build();
    }

    /**
     * Method to request activity data in the last day
     * with quarter-of-hour granularity.
     * Return a {@link DataReadRequest} for the active segments.
     */
    public static DataReadRequest queryActiveTime() {
        Pair<Long, Long> startEndTimes = startEndTimes(1);

        return new DataReadRequest.Builder()
                .aggregate(DataType.TYPE_ACTIVITY_SEGMENT, DataType.AGGREGATE_ACTIVITY_SUMMARY)
                .bucketByTime(15, TimeUnit.MINUTES)
                .setTimeRange(startEndTimes.first, startEndTimes.second, TimeUnit.MILLISECONDS)
                .build();
    }

    /**
     * Method to request nutrition data in the last {@param nDays}
     * with quarter-of-an-hour granularity.
     * Return a {@link DataReadRequest} for the nutrition summaries.
     */
    public static DataReadRequest queryNutritionData(int nDays) {
        Pair<Long, Long> startEndTimes = startEndTimes(nDays);

        return new DataReadRequest.Builder()
                .aggregate(DataType.TYPE_NUTRITION, DataType.AGGREGATE_NUTRITION_SUMMARY)
                .bucketByTime(15, TimeUnit.MINUTES)
                .setTimeRange(startEndTimes.first, startEndTimes.second, TimeUnit.MILLISECONDS)
                .build();
    }

    /**
     * Method to request profile data (weight, height).
     */
    public static DataReadRequest queryProfileData(DataType profileDataType) {
        Pair<Long, Long> startEndTimes = startEndTimes(1);

        //TODO Fix this weird bug. Currently using a hack.
        Calendar cal = Calendar.getInstance();
        cal.set(2015, 6, 25);
        long startTime = cal.getTimeInMillis();

        return new DataReadRequest.Builder()
                .read(profileDataType)
                .setTimeRange(startTime, startEndTimes.second, TimeUnit.MILLISECONDS)
                .setLimit(1)
                .build();
    }

    /**
     * Method to extract profile data (weight, height) from a read result.
     */
    public static double getProfileField(DataReadResult readResult, Field profileField) {
        return readResult.getDataSets().get(0).getDataPoints().get(0)
                .getValue(profileField).asFloat();
    }

    /**
     * Compute top n times (as quarters of an hour) when activityName starts.
     */
    public static List<Long> preferredActivityTimes(DataReadResult readResult, int n, ArrayList<String> activityNames) {
        HashMap<Long, Long> timestamps = new HashMap<>();
        ArrayList<Long> sortedTimestamps = new ArrayList<>();

        for (Bucket bucket : readResult.getBuckets())
            for (DataSet dataSet : bucket.getDataSets())
                for (DataPoint dataPoint : dataSet.getDataPoints())
                    if (activityNames.contains(FitnessActivities.getName(dataPoint.getValue(Field.FIELD_ACTIVITY).asInt())))
                        countTime(timestamps, dataPoint);

        sortTimestamps(timestamps, sortedTimestamps);

        return sortedTimestamps.subList(0, Math.min(timestamps.size(), n));
    }

    /**
     * Compute top n times (as quarters of an hour) when users is having a meal of type {@param mealType}.
     */
    public static List<Long> preferredEatingTimes(DataReadResult readResult, int n, int mealType) {
        HashMap<Long, Long> timestamps = new HashMap<>();
        ArrayList<Long> sortedTimestamps = new ArrayList<>();

        for (Bucket bucket : readResult.getBuckets())
            for (DataSet dataSet : bucket.getDataSets())
                for (DataPoint dataPoint : dataSet.getDataPoints())
                    if(dataPoint.getValue(Field.FIELD_MEAL_TYPE).asInt() == mealType)
                        countTime(timestamps, dataPoint);

        sortTimestamps(timestamps, sortedTimestamps);

        return sortedTimestamps.subList(0, Math.min(timestamps.size(), n));
    }

    /**
     * Return true if the user has eaten the meal of type {@param mealType} today.
     */
    public static boolean hasUserEaten(DataReadResult readResult, int mealType) {
        long startOfToday = getStartOfToday();

        for (Bucket bucket : readResult.getBuckets())
            for (DataSet dataSet : bucket.getDataSets())
                for (DataPoint dataPoint : dataSet.getDataPoints())
                    if (dataPoint.getStartTime(TimeUnit.MILLISECONDS) >= startOfToday &&
                            dataPoint.getValue(Field.FIELD_MEAL_TYPE).asInt() == mealType)
                        return true;
        return false;
    }

    /**
     * Compute the average value of a field in a readResult.
     */
    @SuppressLint("Assert")
    public static long fieldAverage (DataReadResult readResult, Field field, ArrayList<String> activityFilters) {
        assert (readResult.getBuckets().size() > 0 //Data is aggregated
                && (field.getFormat() == Field.FORMAT_FLOAT || field.getFormat() == Field.FORMAT_INT32)); //The value of the field is numerical

        long fieldTotal = 0;

        for (Bucket bucket : readResult.getBuckets())
            for (DataSet dataSet : bucket.getDataSets())
                for (DataPoint dataPoint : dataSet.getDataPoints())
                    if (activityFilters == null || activityFilters.contains(
                            FitnessActivities.getName(dataPoint.getValue(Field.FIELD_ACTIVITY).asInt()))) {
                        if (field.getFormat() == Field.FORMAT_FLOAT)
                            fieldTotal += dataPoint.getValue(field).asFloat();
                        else if (field.getFormat() == Field.FORMAT_INT32)
                            fieldTotal += dataPoint.getValue(field).asInt();
                    }

        return fieldTotal / readResult.getBuckets().size();
    }

    /**
     * Compute the total value of a field in a readResult since the start of the current day.
     */
    public static long fieldCurrentValue (DataReadResult readResult, Field field, ArrayList<String> activityFilters) {
        long startOfToday = getStartOfToday();

        long fieldTotal = 0;
        for (Bucket bucket : readResult.getBuckets())
            for (DataSet dataSet : bucket.getDataSets())
                for (DataPoint dataPoint : dataSet.getDataPoints())
                    if (dataPoint.getStartTime(TimeUnit.MILLISECONDS) >= startOfToday)
                        if (activityFilters == null || activityFilters.contains(
                                FitnessActivities.getName(dataPoint.getValue(Field.FIELD_ACTIVITY).asInt()))) {
                            if (field.getFormat() == Field.FORMAT_FLOAT)
                                fieldTotal += dataPoint.getValue(field).asFloat();
                            else if (field.getFormat() == Field.FORMAT_INT32)
                                fieldTotal += dataPoint.getValue(field).asInt();
                        }

        return fieldTotal;
    }

    /**
     * Compute the total amount of calories in-taken since the start of the current day.
     */
    @SuppressWarnings("ConstantConditions")
    public static long caloriesCurrentTotal (DataReadResult readResult) {
        long startOfToday = getStartOfToday();

        long caloriesTotal = 0;
        for (Bucket bucket : readResult.getBuckets())
            for (DataSet dataSet : bucket.getDataSets())
                for (DataPoint dataPoint : dataSet.getDataPoints())
                    if (dataPoint.getStartTime(TimeUnit.MILLISECONDS) >= startOfToday &&
                        dataPoint.getValue(Field.FIELD_NUTRIENTS).getKeyValue("calories") != null)
                            caloriesTotal += dataPoint.getValue(Field.FIELD_NUTRIENTS).getKeyValue("calories");

        return caloriesTotal;
    }

    /**
     * Log a record of the query result. Should only be used for testing
     * purposes!
     */
    public static void printData(DataReadResult dataReadResult) {
        // If the DataReadRequest object specified aggregated data, dataReadResult will be returned
        // as buckets containing DataSets, instead of just DataSets.
        if (dataReadResult.getBuckets().size() > 0) {
            Log.i(TAG, "Number of returned buckets of DataSets is: "
                    + dataReadResult.getBuckets().size());
            for (Bucket bucket : dataReadResult.getBuckets()) {
                List<DataSet> dataSets = bucket.getDataSets();
                for (DataSet dataSet : dataSets) {
                    dumpDataSet(dataSet);
                }
            }
        } else if (dataReadResult.getDataSets().size() > 0) {
            Log.i(TAG, "Number of returned DataSets is: "
                    + dataReadResult.getDataSets().size());
            for (DataSet dataSet : dataReadResult.getDataSets()) {
                dumpDataSet(dataSet);
            }
        }
    }

    //Compute a range of past n Days
    private static Pair<Long, Long> startEndTimes (int nDays) {
        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);
        long endTime = cal.getTimeInMillis();
        cal.add(Calendar.DAY_OF_WEEK, -nDays);
        long startTime = cal.getTimeInMillis();

        return new Pair<>(startTime, endTime);
    }

    //UNIX Time of the start of the current day
    private static long getStartOfToday() {
        Calendar cal = Calendar.getInstance();
        return new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)).getTimeInMillis();
    }

    //Record the start time (in quarters of an hour since 12AM)
    //of a data point inside a counting hashmap.
    private static void countTime(HashMap<Long, Long> timestamps, DataPoint dataPoint) {
        long key = (dataPoint.getStartTime(TimeUnit.MINUTES) % (24 * 60)) / 15;
        if (timestamps.containsKey(key))
            timestamps.put(key, timestamps.get(key) + 1);
        else timestamps.put(key, (long) 1);
    }

    @SuppressWarnings("unchecked")
    //Sort a hashmap of timestamps into a list.
    private static void sortTimestamps(HashMap<Long, Long> timestamps, ArrayList<Long> sortedTimestamps) {
        List hashEntries = new LinkedList(timestamps.entrySet());
        // Defined Custom Comparator here
        Collections.sort(hashEntries, new Comparator() {
            public int compare(Object o1, Object o2) {
                return -((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        });

        for (Object hashEntry : hashEntries) {
            Map.Entry entry = (Map.Entry) hashEntry;
            sortedTimestamps.add((Long) entry.getKey());
        }
    }

    private static void dumpDataSet(DataSet dataSet) {
        Log.i(TAG, "Data returned for Data type: " + dataSet.getDataType().getName());
        DateFormat dateFormat = getDateTimeInstance();

        for (DataPoint dp : dataSet.getDataPoints()) {
            Log.i(TAG, "Data point:");
            Log.i(TAG, "\tType: " + dp.getDataType().getName());
            Log.i(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
            Log.i(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));
            for(Field field : dp.getDataType().getFields()) {
                Log.i(TAG, "\tField: " + field.getName() +
                        " Value: " + dp.getValue(field));
            }
        }
    }

    public interface FitDataRequester {
        void onDataFetched (ArrayList<DataReadResult> dataReadResults);
    }
}
