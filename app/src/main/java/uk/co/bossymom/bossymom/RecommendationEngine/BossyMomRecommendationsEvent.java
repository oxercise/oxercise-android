package uk.co.bossymom.bossymom.RecommendationEngine;

import uk.co.bossymom.bossymom.CalendarTools.BossyMomCalendarEvent;

/**
 * Internal format for recommendations given by the app extends internal format for events from user calendar.
 */
public class BossyMomRecommendationsEvent extends BossyMomCalendarEvent {
    public RecommendationTypes activityType;
    public String data;
    public RecommendationTypes extendedType;

    // For insertion of regular events
    public BossyMomRecommendationsEvent(long begin, long end, String title, RecommendationTypes type){
        super(-1, begin, end, title, true, false, false);
        activityType = type;
        extendedType = type;
    }

    // For insertion of meal type events
    public BossyMomRecommendationsEvent(long begin, long end, String title, RecommendationTypes type, String data){
        super(-1, begin, end, title, true, false, false);
        activityType = type;
        extendedType = type;
        this.data = data.toString();
    }

    public void compressType() {
        switch (activityType) {
            case BREAKFAST:
            case LUNCH:
            case DINNER:
                activityType = RecommendationTypes.MEAL;
            default:
                break;
        }
    }

}


