package uk.co.bossymom.bossymom.CalendarTools;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.provider.CalendarContract.Instances;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;

import uk.co.bossymom.bossymom.RecommendationEngine.BossyMomRecommendationsEvent;
import uk.co.bossymom.bossymom.RecommendationEngine.RecommendationTypes;

/**
 * Class to implement reading / writing from the calendar.
 * Class is initialised by the content resolver.
 */
public class BossyMomCalendar extends AppCompatActivity {
    private ContentResolver resolver;
    private Context context;
    private long primaryCalendarID = -1;

    // Saving events
    public static final String PREFS_NAME = "BossyMomRecommendedCalendarEvents";
    public static final String EVENTS_IDS_PREF_NAME = "recommendationsIDs";

    // For debugging
    private String LOG_TAG = "BossyMomCalendar";

    public BossyMomCalendar(ContentResolver res, Context context) {
        resolver = res;
        this.context = context;
    }

    private Calendar today0000() {
        // today
        Calendar date = Calendar.getInstance();
        // reset hour, minutes, seconds and millis
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        return date;
    }

    private long today0000Milliseconds() {
        // today
        Calendar date = today0000();
        return date.getTimeInMillis();
    }

    private long tomorrow0000Milliseconds() {
        // today
        Calendar date = today0000();
        // next day
        date.add(Calendar.DAY_OF_MONTH, 1);
        return date.getTimeInMillis();
    }

    /**
     * Get names of user calendars.
     * @return a list of titles of user calendars.
     */
    Vector<String> getCalendars() {
        String[] projection =
                new String[]{
                        Calendars._ID,
                        Calendars.NAME,
                        Calendars.IS_PRIMARY,
                        Calendars.ACCOUNT_NAME,
                        Calendars.ACCOUNT_TYPE,};

        try {
            Cursor calCursor = resolver.query(Calendars.CONTENT_URI,
                    projection,
                    Calendars.VISIBLE + " = 1",
                    null,
                    Calendars._ID + " ASC");

            Vector<String> calendars = new Vector<>();
            if (calCursor.moveToFirst()) {
                do {
                    long id = calCursor.getLong(0);
                    long prime = calCursor.getLong(2);
                    if(prime == 1 && primaryCalendarID == -1)
                        primaryCalendarID = id;
                    String displayName = calCursor.getString(1);

                    calendars.addElement(displayName);

                } while (calCursor.moveToNext());
            }
            calCursor.close();
            return calendars;
        } catch (SecurityException s) {
            throw new RuntimeException("No permissions to read calendar");
        }

    }

    /**
     * Return a vector of internally used calendarEvents happening between 00:00:00 and 23:59:59
     * on a current day, measured as in the user timezone.
     * @return a vector of internal format of calendar events.
     * */
    public Vector<BossyMomCalendarEvent> getTodaysEvents() {
        long today = today0000Milliseconds();
        long tomorrow = tomorrow0000Milliseconds();
        return getEventsBetween(today, tomorrow);
    }

    /**
     *  Return a vector of internally used calendarEvents happening between times specified by
     *  given java date objects.
     *  @param beginDate is starting time of this interval, given in java.util.Calendar date object
     *  @param endDate is ending time of this interval, given in java.util.Calendar date object
     *  @return a vector of internal format of calendar events.
     * */
    public Vector<BossyMomCalendarEvent> getEventsBetween(Calendar beginDate, Calendar endDate) {
        long beginMs = beginDate.getTimeInMillis();
        long endMs   = endDate.getTimeInMillis();
        return getEventsBetween(beginMs, endMs);
    }

    /**
     * Return a vector of pairs of Calendars denoting free slots in the agenda with starting times
     * between current time and 23:59:59 on a current day, measured as in the user timezone.
     * The last event might go on to the next day (primarily intended for managing sleep).
     * @return a vector of pairs of java Calendars, denoting free time intervals
     * */
    public Vector<Pair<Calendar, Calendar>> getFreeSlotsFromNow() {
        Calendar now = Calendar.getInstance();
        // Tomorrow is today + 1 day
        Calendar tomorrow = today0000();
        tomorrow.add(Calendar.DAY_OF_MONTH, 1);

        return getFreeTime(now, tomorrow);
    }

    /**
     * Return a vector of pairs of Calendars denoting free slots in the agenda with starting times
     * between 00:00:00 and 23:59:59 on a current day, measured as in the user timezone.
     * The last event might go on to the next day (primarily intended for managing sleep).
     * @return a vector of pairs of java Calendars
     * */
    public Vector<Pair<Calendar, Calendar>> getTodaysFreeSlots() {
        Calendar today = today0000();
        // Tomorrow is today + 1 day
        Calendar tomorrow = today0000();
        tomorrow.add(Calendar.DAY_OF_MONTH, 2);

        return getFreeTime(today, tomorrow);
    }

    /**
     * Return a vector of pairs of Calendars denoting free slots in the agenda with starting times
     * in a given interval.
     * The last event might go on to the next day (primarily intended for managing sleep).
     * @param beginDate is starting time of this interval, given in java.util.Calendar date object
     * @param endDate is ending time of this interval, given in java.util.Calendar date object
     * @return a vector of pairs of java Calendars
     * */
    public Vector< Pair<Calendar, Calendar> > getFreeTime(Calendar beginDate, Calendar endDate) {
        long beginMs = beginDate.getTimeInMillis();
        long endMs   = endDate.getTimeInMillis();

        Calendar includingSleep = (Calendar) endDate.clone();
        // next day
        includingSleep.add(Calendar.DAY_OF_MONTH, 1);
        long endMsWithSleep   = includingSleep.getTimeInMillis();
        // Get events list in this interval
        Vector<BossyMomCalendarEvent> todaysEventList = getEventsBetween(beginMs, endMsWithSleep);

        // Sort by starting time
        Collections.sort(todaysEventList);

        // Prepare to collect results
        Vector<Pair<Calendar, Calendar>> freeSlots = new Vector<>();

        // Store the earliest time currently known to be free
        long lastestEndingMs = beginMs;
        // Iterate through events in a given time
        for(BossyMomCalendarEvent bmce : todaysEventList)
            if (!bmce.allDay && !bmce.availability){
                // Check that event is starting later than latest known event end time, thus creating
                // a free slot (and also check that this free slot begins in a given period)
                if(bmce.beginUTCms > lastestEndingMs && lastestEndingMs < endMs){
                    Calendar intervalStart = Calendar.getInstance();
                    Calendar intervalEnd = Calendar.getInstance();

                    intervalStart.setTimeInMillis(lastestEndingMs);
                    intervalEnd.setTimeInMillis(bmce.beginUTCms);

                    freeSlots.addElement( Pair.create( intervalStart, intervalEnd));
                }
                // For all events, update their ending times
                lastestEndingMs = Math.max(lastestEndingMs, bmce.endUTCms);
            }

        if(lastestEndingMs < endDate.getTimeInMillis()){
            Calendar intervalStart = Calendar.getInstance();
            intervalStart.setTimeInMillis(lastestEndingMs);
            freeSlots.addElement( Pair.create( intervalStart, endDate));
        }


        return freeSlots;
    }

    /**
     *  Return a vector of internally used calendarEvents happening between times specified
     *  in UTC milliseconds since the epoch.
     *  @param begin is starting time of this interval, in UTC milliseconds since the epoch.
     *  @param end is ending time of this interval, in UTC milliseconds since the epoch.
     *  @return a vector of internal format of calendar events.
     * */
    public Vector<BossyMomCalendarEvent> getEventsBetween(long begin, long end) {
        String[] proj = new String[]{
                Instances._ID,
                Instances.BEGIN,
                Instances.END,
                Instances.TITLE,
                Instances.VISIBLE,
                Instances.ALL_DAY,
                Instances.AVAILABILITY,
                Instances.EVENT_ID
        };

        Cursor cursor = Instances.query(resolver, proj, begin, end);

        Vector<BossyMomCalendarEvent> queriedEvents = new Vector<BossyMomCalendarEvent>();
        if (cursor.moveToFirst()) {
            do {
                Log.d(LOG_TAG, "ID " + cursor.getLong(0) + " eventID " + cursor.getLong(7) + ", title: " + cursor.getString(3));
                BossyMomCalendarEvent ev = new BossyMomCalendarEvent(cursor.getInt(0),
                        cursor.getLong(1),
                        cursor.getLong(2),
                        cursor.getString(3),
                        cursor.getInt(4) == 1,
                        cursor.getInt(5) == 1,
                        cursor.getInt(6) == 1);
                if (!ev.allDay)
                    queriedEvents.addElement(ev);
            } while (cursor.moveToNext());
        }
        return queriedEvents;
    }

    /** Write all given BossyMomRecommendationsEvent s to user's primary calendar.
     *
     * @param recommendationsEvents is a list of recommended events.
     */
    public List<Long> writeRecommendations(Vector<BossyMomRecommendationsEvent> recommendationsEvents){
        ArrayList<Long> EventIDs = new ArrayList<>();
        for(BossyMomRecommendationsEvent recommendation : recommendationsEvents) {
            long id = writeEvent(recommendation);
            EventIDs.add(id);
            Log.d(LOG_TAG, "Written id: " + id);
        }

        saveIDs(EventIDs);
        return EventIDs;
    }

    /**
     * Delete all the recommendations made by the app (stored in shared preferences) from the calendar.
     */
    public void deleteAllRecommendations(){
        // Define range of recommendations
        long now = today0000().getTimeInMillis();

        Calendar future = today0000();
        future.add(Calendar.DAY_OF_MONTH, 2);
        long until = future.getTimeInMillis();

        deleteFutureRecommendationsFromRange(now, until, new ArrayList<Long>());
    }

    /**
     * Delete all the future recommendations made by the app (stored in shared preferences) from the calendar.
     */
    public void deleteFutureRecommendations( List<Long> toKeep ){
        // Define range of recommendations
        long now = Calendar.getInstance().getTimeInMillis();

        Calendar future = Calendar.getInstance();
        future.add(Calendar.DAY_OF_MONTH, 1);
        long until = future.getTimeInMillis();

        deleteFutureRecommendationsFromRange(now, until, toKeep);
    }

    private void deleteFutureRecommendationsFromRange(long now, long until, List<Long> toKeep){
        // Get all events ID from now until next day
        String[] proj = new String[]{Instances.EVENT_ID};
        Cursor cursor = Instances.query(resolver, proj, now, until);

        // Saved recommendations IDs
        ArrayList<Long> recommendationIDs = restoreIDs();
        ArrayList<Long> recommendationIDsToDelete = new ArrayList<Long>();

        Log.d(LOG_TAG, "TO KEEP" + toKeep.toString());

        if (cursor.moveToFirst()) {
            do {
                long eventID = cursor.getInt(0);
                if(recommendationIDs.contains(eventID) && ! toKeep.contains(eventID))
                    recommendationIDsToDelete.add(eventID);

            } while (cursor.moveToNext());
        }
        cursor.close();
        deleteEvents(recommendationIDsToDelete);
        removeRecommendationsFromSharedPrefs(recommendationIDsToDelete);
    }

    /**
     * Remove given list of events (identified by EventID in the calendar) from shared preferences.
     * @param IDsToDelete is the list of calendar event IDs to delete.
     */
    private void removeRecommendationsFromSharedPrefs(ArrayList<Long> IDsToDelete){
        ArrayList<Long> allRecommendations = restoreIDs();
        allRecommendations.removeAll(IDsToDelete);
        saveGivenIDsToSharedPrefs(allRecommendations, "");
    }

    /**
     * Write given IDs, concatenated with string previouslySaved. (previouslySaved + IDs)
     * @param IDs List of event IDs to be stored
     * @param previouslySaved String to be prepended to the list of IDs
     */
    private void saveGivenIDsToSharedPrefs(ArrayList<Long> IDs, String previouslySaved){
        String ids = "";

        for (Long eventID : IDs)
            ids += eventID + ",";

        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);

        SharedPreferences.Editor editor = settings.edit();
        editor.putString(EVENTS_IDS_PREF_NAME, previouslySaved + ids);

        editor.commit();
    }

    /**
     * Append the given event IDs to stored IDs of recommendation events.
     * @param IDs List of event IDs to be stored
     */
    private void saveIDs(ArrayList<Long> IDs){
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        String pastRecommendations = settings.getString(EVENTS_IDS_PREF_NAME, "");

        saveGivenIDsToSharedPrefs(IDs, pastRecommendations);
    }


    private String getPrefsName(RecommendationTypes type){
        String prefsID = "previousRecommendationsFor";
        switch (type){
            case BREAKFAST: prefsID += "Breakfast"; break;
            case LUNCH:     prefsID += "Lunch"; break;
            case DINNER:    prefsID += "Dinner"; break;
            case EXERCISE:  prefsID += "Exercise"; break;
            case SLEEP:     prefsID += "Sleep"; break;
        }
        return prefsID;
    }

    public void saveRecommendation(BossyMomRecommendationsEvent event){
        String pref = getPrefsName(event.activityType);

        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();

        editor.putLong  (pref + "begin", event.beginUTCms);
        editor.putLong  (pref + "end",   event.endUTCms);
        editor.putString(pref + "title", event.title);
        editor.putString(pref + "data",  event.data);

        editor.commit();
    };

    public List<Long> restorePreviousRecommendation(RecommendationTypes type){
        String pref = getPrefsName(type);
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);

        long begin      = settings.getLong  (pref + "begin", -1);
        long end        = settings.getLong  (pref + "end", -1);
        String title    = settings.getString(pref + "title", "");
        String data     = settings.getString(pref + "data", "");

        Log.d(LOG_TAG, "restoring " + title + " for type " + type.name());
        // If it is not a default value, write it to the calendar
        if(begin > -1){
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(begin);
            Log.d(LOG_TAG, "" + c.getTime());

            Vector<BossyMomRecommendationsEvent> ev = new Vector<>();
            ev.add(new BossyMomRecommendationsEvent(begin, end, title, type, data));
            ev.firstElement().compressType();

            return writeRecommendations(ev);
        }
        return (new ArrayList<>());
    }


    /** Get the stored recommendation IDs from the shared preferences.
     * @return A list of IDs in numerical format.
     */
    protected ArrayList<Long> restoreIDs(){
        ArrayList<Long> numericalIDs = new ArrayList<>();

        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        String pastRecommendations = settings.getString(EVENTS_IDS_PREF_NAME, "");

        String[] stringIDs = pastRecommendations.split(",");
        for(String s : stringIDs){
            if(s != "")
                numericalIDs.add(Long.parseLong(s, 10));
        }
        return numericalIDs;
    }

    /** Delete events from the calendar by given EventIDs
     * @param events EventIDs of events to be deleted.
     */
    private void deleteEvents(ArrayList<Long> events){
        if(primaryCalendarID == -1)
            getCalendars();

        for(Long eventID : events) {
            Uri deleteUri = ContentUris.withAppendedId(Events.CONTENT_URI, eventID);
            resolver.delete(deleteUri, null, null);
        }

    }

    /**
     * Insert an event to a calendar.
     * @param cevent is an event in internal format to be stored in a primary calendar of a user.
     * Note that there is no "the" primary calendar, so it is stored in one of those.
     * @return an ID of an inserted event (can be safely ignored).
     * */
    public long writeEvent(BossyMomRecommendationsEvent cevent) {
        Log.d(LOG_TAG, "Before: PrimaryID" + primaryCalendarID);
        if(primaryCalendarID == -1)
            getCalendars();
        Log.d(LOG_TAG, "After: PrimaryID" + primaryCalendarID);


        try{
            ContentResolver cr = resolver;
            ContentValues values = new ContentValues();
            values.put(Events.DTSTART, cevent.beginUTCms);
            values.put(Events.DTEND, cevent.endUTCms);
            values.put(Events.TITLE, cevent.title);
            values.put(Events.CALENDAR_ID, primaryCalendarID);
            values.put(Events.EVENT_TIMEZONE, TimeZone.getDefault().getDisplayName());

            switch (cevent.activityType){
                case MEAL:
                    values.put(Events.DESCRIPTION, "Calories: " + cevent.data + " kcal");
                    break;
                case EXERCISE:
                    values.put(Events.DESCRIPTION, "Calories to be burnt: " + cevent.data + " kcal");
                    break;
            }

            Uri uri = cr.insert(Events.CONTENT_URI, values);

            long eventID = Long.parseLong(uri.getLastPathSegment());
            //Log.d(TAG, "Event inserted.");
            return eventID;
        }
        catch (SecurityException s) {
            throw new RuntimeException("No permissions to write calendar");
        }
    }
}
