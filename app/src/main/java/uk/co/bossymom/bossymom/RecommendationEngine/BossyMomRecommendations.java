package uk.co.bossymom.bossymom.RecommendationEngine;

import android.content.ContentResolver;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;

import com.google.android.gms.fitness.FitnessActivities;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.result.DataReadResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;

import uk.co.bossymom.bossymom.CalendarTools.BossyMomCalendar;
import uk.co.bossymom.bossymom.DataTools.FitDataHandler;
import uk.co.bossymom.bossymom.DataTools.GetEventsAsyncTask;
import uk.co.bossymom.bossymom.MainActivity;
import uk.co.bossymom.bossymom.NotificationsTools.NotificationManager;

/**
 * Class to offer recommendations for the rest of the day
 */

/*
BUGS:
1. Not terminating properly sometimes?
2. If user preferred time is after midnight, recommendations go to previous day.
 */

public class BossyMomRecommendations extends FragmentActivity implements FitDataHandler.FitDataRequester {

    public static final String LOG_TAG = "BossyMomRecommendations";
    private boolean recommendInBackground;

    private Vector<Pair<Calendar, Calendar>> freeSlots;
    private BossyMomRecommendedAmounts optimal;

    private ContentResolver resolver;
    private AppCompatActivity callerActivity;
    private Context context;

    /** TENTATIVE class initialisation -- most likely to change once the precise format of averages
     * and recommended amounts is going to be resolve
     */
    public BossyMomRecommendations(AppCompatActivity callerActivity, ContentResolver res){
        resolver = res;
        this.callerActivity = callerActivity;
        this.context = callerActivity;
        recommendInBackground = false;

        // Get optimal amounts of sleep / exercise / eating
        optimal = new BossyMomRecommendedAmounts();
    }

    //To be used for background calls
    public BossyMomRecommendations(Context context, ContentResolver res){
        resolver = res;
        this.context = context;
        recommendInBackground = true;

        // Get optimal amounts of sleep / exercise / eating
        optimal = new BossyMomRecommendedAmounts();
    }

    private Boolean isFree(Calendar from, Integer durationInMin){
        Calendar until = (Calendar) from.clone();
        until.add(Calendar.MINUTE, durationInMin);

        for( Pair<Calendar, Calendar> interval : freeSlots)
            if(interval.first.getTimeInMillis() <= from.getTimeInMillis() && until.getTimeInMillis() <= interval.second.getTimeInMillis())
                return true;
        return false;
    }

    private Calendar fifteenMinutesToCalendar(Long quarters){
        Calendar date = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        date.add(Calendar.MINUTE, (int) (quarters * 15));

        //date.setTimeZone(TimeZone.getDefault());
        return date;
    }

    private boolean doesNotClash(Vector<BossyMomRecommendationsEvent> previousRecommendations,
                                 Calendar from, Integer durationInMin){
        Calendar until = (Calendar) from.clone();
        until.add(Calendar.MINUTE, durationInMin);

        for( BossyMomRecommendationsEvent pastEvent : previousRecommendations )
            if( pastEvent.beginUTCms <= until.getTimeInMillis() &&  from.getTimeInMillis() <= pastEvent.endUTCms )
                return false;

        return true;
    }

    private Vector<BossyMomRecommendationsEvent> recommendActivity(List<Long> preferredActivityList,
                                                                   Vector<BossyMomRecommendationsEvent> previousRecommendations,
                                                                   Integer recommendedAmount,
                                                                   String title,
                                                                   RecommendationTypes type,
                                                                   String data){
        Vector<BossyMomRecommendationsEvent> recommendations = new Vector<>();

        Long mostPreferredTime = (long) 0;
        Long endOfTheDay = (long) 24 * 60 / 15;
        if(preferredActivityList.size() > 0)
            mostPreferredTime = preferredActivityList.get(0);

        List<Long> restOfTheDay = new ArrayList<>();
        for(long i = mostPreferredTime; i < endOfTheDay; i += 1)
            restOfTheDay.add(i);
        preferredActivityList.addAll(restOfTheDay);


        Calendar now = Calendar.getInstance();
        for(Long timeQuarters : preferredActivityList ){
            Calendar time = fifteenMinutesToCalendar(timeQuarters);
            Log.d(LOG_TAG, "Time: " + time.getTime() + ", now: " + now.getTime() + " times diff " + (now.getTimeInMillis() <= time.getTimeInMillis()) );
            Log.d(LOG_TAG, "amount: " + recommendedAmount);
            Log.d(LOG_TAG, "isFree" + isFree(time, recommendedAmount));
            Log.d(LOG_TAG, "doesNotClash: " + doesNotClash(previousRecommendations, time, recommendedAmount));

            if(now.getTimeInMillis() <= time.getTimeInMillis() && isFree(time, recommendedAmount) && doesNotClash(previousRecommendations, time, recommendedAmount) ) {
                long begin = time.getTimeInMillis();
                Calendar until = (Calendar) time.clone();
                until.add(Calendar.MINUTE, recommendedAmount);
                Log.d(LOG_TAG, "Recommended Amount:" + recommendedAmount);

                long end   = until.getTimeInMillis();
                Log.d(LOG_TAG, "End time: " + until.getTime());

                BossyMomRecommendationsEvent recommendedEvent = new BossyMomRecommendationsEvent(begin, end, title, type, data);
                recommendations.add(recommendedEvent);

                return recommendations;
            }
        }

        return recommendations;
    }

    private void recommendSleep(Vector<BossyMomRecommendationsEvent> recommendations, double sleepAverage,
                                long sleepToday, List<Long> preferredSleep){
        Double sleepDebt = (optimal.sleep() - sleepAverage);
        Integer maxCatchUpTime = 60 * 3/2; // Do not attempt to variate sleep schedule by more than this

        Log.d(LOG_TAG, "Sleep debt: " + sleepDebt);
        Log.d(LOG_TAG, "Sleep today: " + sleepToday);


        Integer additional = (int) (long) (Math.round(sleepDebt * maxCatchUpTime / (3*60) ));
        if(additional < 0)
            additional = 0;
        if(additional > maxCatchUpTime)
            additional = maxCatchUpTime;

        Log.d(LOG_TAG, "additional: " + additional);

        Integer recommendedAmount = optimal.sleep() + additional;
        // Round to 15 minutes intervals
        recommendedAmount = 15 * (int) (long) Math.round( (double)recommendedAmount / 15.0);
        String hours = Math.round(recommendedAmount / 60) + " h " + recommendedAmount % 60 + " min";

        recommendations.addAll(recommendActivity(preferredSleep, recommendations, recommendedAmount, "Sleep",
                RecommendationTypes.SLEEP, hours));
    }

    private double BMI(double weight, double height){
        return weight / height / height;
    }

    private Integer recommendExercise(Vector<BossyMomRecommendationsEvent> recommendations, double weight,
                                      double height, Integer aveActiveTime, List<Long> preferredExercise,
                                      double activeTimeToday){
        double bmi = BMI(weight, height);
        Integer maxCatchUpTime = 30; // Do not attempt to variate exercise time by more than this
        Integer debt = optimal.exercise() - aveActiveTime;

        double bmiInducedAdditionalTime = ((bmi - 25) / 5) * maxCatchUpTime;
        double inactivityInducedAdditionalTime = (double) (debt / 60)  * maxCatchUpTime;

        Integer additional = (int) (long) (Math.round(( bmiInducedAdditionalTime + inactivityInducedAdditionalTime )));
        if(additional < 0)
            additional = 0;
        if(additional > maxCatchUpTime)
            additional = maxCatchUpTime;

        Integer recommendedAmount = optimal.exercise() + additional - (int) (long) Math.round(activeTimeToday);
        Log.d(LOG_TAG, "Recommended amount for exercise: " + recommendedAmount);

        Integer expectedCaloriesToBeBurnt = optimal.caloriesBurned(recommendedAmount, weight);

        // If needed, recommend an event and return burned calories with the event
        if(recommendedAmount > 0){
            recommendations.addAll(recommendActivity(preferredExercise, recommendations, recommendedAmount,
                    "Running", RecommendationTypes.EXERCISE, expectedCaloriesToBeBurnt.toString()));
            return expectedCaloriesToBeBurnt;
        }
        else
            return 0;
    }

    private void recommendEating(Vector<BossyMomRecommendationsEvent> recommendations,
                                 Integer calories, List<Long> preferredTimes, String mealType, RecommendationTypes type){
        Integer recommendedAmount = 15;

        recommendations.addAll(recommendActivity(preferredTimes, recommendations,
                recommendedAmount, mealType, type, calories.toString()));
    }

    private Integer caloriesNeededToday(double todayExpandedCalories, double averageExpandedCalories,
                                       double weight, double height,
                                       Integer caloriesExpectedToBeBurned, long caloriesConsumedToday){
        Calendar now = Calendar.getInstance();
        double percentageOfDayPassed = (double) (now.get(Calendar.MINUTE) + now.get(Calendar.HOUR_OF_DAY)*60) / (24*60);
        Log.d(LOG_TAG, "% of day " + percentageOfDayPassed);

        double caloriesNeededToday = todayExpandedCalories + (1 - percentageOfDayPassed)*averageExpandedCalories;

        // Adjustment for losing weight
        caloriesNeededToday -= (BMI(weight, height) > 25 ? 500 : 0);

        caloriesNeededToday += caloriesExpectedToBeBurned - caloriesConsumedToday;

        if(caloriesNeededToday > 0)
            return (int) (long) Math.round(caloriesNeededToday);
        else
            return 0;
    }

    public void recommend() {
        /// Get today's free time slots
        BossyMomCalendar cal = new BossyMomCalendar(resolver, context);
        //Invalidate future recommendations
        cal.deleteAllRecommendations();
        // Get today's free slots
        freeSlots = cal.getTodaysFreeSlots();

        for(Pair<Calendar,Calendar> slot : freeSlots)
            Log.d(LOG_TAG, "FREE from " + slot.first.getTime() + " to " + slot.second.getTime());

        Log.d(LOG_TAG, "Events from Calendar are fine");

        // Request data from Google Fit asynchronously
        FitDataHandler fitDataHandler;
        if (recommendInBackground) fitDataHandler = new FitDataHandler(context);
        else fitDataHandler = new FitDataHandler(callerActivity);

        fitDataHandler.buildFitnessClient(
                fitDataHandler.generateQueryTask(this,
                        FitDataHandler.queryExpendedCalories(14), FitDataHandler.queryActiveTime(21),
                        FitDataHandler.queryNutritionData(14), FitDataHandler.queryProfileData(DataType.TYPE_WEIGHT),
                        FitDataHandler.queryProfileData(DataType.TYPE_HEIGHT), FitDataHandler.queryActiveTime(),
                        FitDataHandler.queryExpendedCalories())
        );
    }

    @Override
    public void onDataFetched(ArrayList<DataReadResult> dataReadResults) {
        new ComputeRecommendationsAsyncTask(dataReadResults).execute();
    }

    //AsyncTask to compute the recommendation data on a different thread from the UI.
    private class ComputeRecommendationsAsyncTask extends AsyncTask<Void, Void, Pair<Vector<BossyMomRecommendationsEvent>, List<Long>>> {

        ArrayList<DataReadResult> dataReadResults;
        BossyMomCalendar bmCalendar = new BossyMomCalendar(resolver, context);


        private List<Long> restoreUnchangedNotifications(Vector<BossyMomRecommendationsEvent> recommendations){
            List<RecommendationTypes> generatedTypes = new ArrayList<>();

            for(BossyMomRecommendationsEvent recommendationsEvent : recommendations){
                // Save newest recommendation and check that this type has been mentioned
                bmCalendar.saveRecommendation(recommendationsEvent);
                generatedTypes.add(recommendationsEvent.activityType);

                // Pack values of meals
                recommendationsEvent.compressType();
            }

            List<Long> restoredIDs = new ArrayList<>();
            for(RecommendationTypes type : RecommendationTypes.values())
                if( type != RecommendationTypes.MEAL && !generatedTypes.contains(type) ){
                    Log.d(LOG_TAG, "Want to restore type" + type.name());
                    restoredIDs.addAll(bmCalendar.restorePreviousRecommendation(type));
                }
            return restoredIDs;
        }


        public ComputeRecommendationsAsyncTask(ArrayList<DataReadResult> dataReadResults) {
            this.dataReadResults = dataReadResults;
        }

        @Override
        protected Pair<Vector<BossyMomRecommendationsEvent>, List<Long>> doInBackground(Void... params) {
            //Invalidate future recommendations
            //TODO Overwrite only the updated ones
            //bmCalendar.deleteFutureRecommendations();

            //Analyse sleep data and generate sleep recommendations
            SleepAnalyser sleepAnalyser = new SleepAnalyser(dataReadResults).invoke();
            Vector<BossyMomRecommendationsEvent> recommendations = sleepAnalyser.recommendations;

            double weight = FitDataHandler.getProfileField(dataReadResults.get(3), Field.FIELD_WEIGHT);
            double height = FitDataHandler.getProfileField(dataReadResults.get(4), Field.FIELD_HEIGHT);

            //Analyse active time
            ActiveTimeAnalyser activeTimeAnalyser = new ActiveTimeAnalyser(dataReadResults).invoke();
            double aveActiveTime = activeTimeAnalyser.aveActiveTime;
            List<Long> preferredExercise = activeTimeAnalyser.preferredExercise;
            double activeTimeToday = activeTimeAnalyser.activeTimeToday;

            //Analyse nutrition data and recommend meals and exercise
            analyseNutritionData(dataReadResults, recommendations, weight,
                    height, aveActiveTime, preferredExercise, activeTimeToday);

            Log.d(LOG_TAG, "Recommendations are done, count: " + recommendations.size());

            List<Long> eventIDsToKeep = restoreUnchangedNotifications(recommendations);
            return Pair.create(recommendations, eventIDsToKeep);
        }

        @Override
        protected void onPostExecute(Pair<Vector<BossyMomRecommendationsEvent>, List<Long>> bossyMomRecommendationsEvents) {
            setNotifications(bossyMomRecommendationsEvents.first);

            if (!recommendInBackground) {
                //Recommendations are done. Post them to the EventsList populator.
                MainActivity mainActivity = (MainActivity) callerActivity;
                new GetEventsAsyncTask(mainActivity, bossyMomRecommendationsEvents.first, bossyMomRecommendationsEvents.second).execute();
            }

        }
    }

    //Set notifications for the recommendations computed
    private void setNotifications(Vector<BossyMomRecommendationsEvent> bossyMomRecommendationsEvents) {
        NotificationManager nm = new NotificationManager(context);

        //Clean the previously set notifications
        nm.cancelAllNotifications();

        for (BossyMomRecommendationsEvent recommendation : bossyMomRecommendationsEvents)
            nm.scheduleRecommendedNotification(recommendation);
    }

    //Analyse sleep data and recommend sleep.
    private class SleepAnalyser {
        private ArrayList<DataReadResult> dataReadResults;
        protected ArrayList<String> sleepStartActivities;
        protected Vector<BossyMomRecommendationsEvent> recommendations;

        public SleepAnalyser(ArrayList<DataReadResult> dataReadResults) {
            this.dataReadResults = dataReadResults;
        }

        public SleepAnalyser invoke() {
            long sleepAverage =  FitDataHandler.fieldAverage(dataReadResults.get(1), Field.FIELD_DURATION,
                    new ArrayList<>(Arrays.asList(new String[]{
                            FitnessActivities.SLEEP_DEEP,
                            FitnessActivities.SLEEP_AWAKE,
                            FitnessActivities.SLEEP_LIGHT,
                            FitnessActivities.SLEEP_REM,
                            FitnessActivities.SLEEP
                    })));

            long sleepToday = FitDataHandler.fieldCurrentValue(dataReadResults.get(1), Field.FIELD_DURATION,
                    new ArrayList<>(Arrays.asList(new String[]{
                            FitnessActivities.SLEEP_DEEP,
                            FitnessActivities.SLEEP_AWAKE,
                            FitnessActivities.SLEEP_LIGHT,
                            FitnessActivities.SLEEP_REM,
                            FitnessActivities.SLEEP
                    })));

            sleepStartActivities = new ArrayList<>();
            sleepStartActivities.add(FitnessActivities.SLEEP);
            sleepStartActivities.add(FitnessActivities.SLEEP_LIGHT);
            List<Long> preferredSleep = FitDataHandler.preferredActivityTimes(dataReadResults.get(1), 10, sleepStartActivities);

            Log.d(LOG_TAG, "Sleep average: " + sleepAverage);
            Log.d(LOG_TAG, "Sleep today: " + sleepToday );

            recommendations = new Vector<>();
            recommendSleep(recommendations, sleepAverage / 1000 / 60, sleepToday / 1000 / 60, preferredSleep);

            return this;
        }
    }

    //Analyse active time data
    private class ActiveTimeAnalyser {
        private ArrayList<DataReadResult> dataReadResults;
        protected double aveActiveTime;
        protected List<Long> preferredExercise;
        protected double activeTimeToday;

        public ActiveTimeAnalyser(ArrayList<DataReadResult> dataReadResults) {
            this.dataReadResults = dataReadResults;
        }

        public ActiveTimeAnalyser invoke() {
            // In milliseconds,
            aveActiveTime = FitDataHandler.fieldAverage(dataReadResults.get(1), Field.FIELD_DURATION,
                    new ArrayList<>(Arrays.asList(new String[]{
                            FitnessActivities.WALKING,
                            FitnessActivities.RUNNING,
                            FitnessActivities.BIKING,
                    })));

            ArrayList<String> queriedActivities = new ArrayList<>();
            queriedActivities.add(FitnessActivities.RUNNING);
            //queriedActivities.add(FitnessActivities.WALKING); //--Walking might be worth analysing?
            preferredExercise = FitDataHandler.preferredActivityTimes(dataReadResults.get(1), 10, queriedActivities);

            activeTimeToday = FitDataHandler.fieldCurrentValue(dataReadResults.get(5), Field.FIELD_DURATION,
                    new ArrayList<>(Arrays.asList(new String[]{
                            FitnessActivities.WALKING,
                            FitnessActivities.RUNNING,
                            FitnessActivities.BIKING,
                    })));
            return this;
        }
    }

    //Analyse nutrition data and recommend Exercise and Eating.
    private void analyseNutritionData(ArrayList<DataReadResult> dataReadResults,
                                      Vector<BossyMomRecommendationsEvent> recommendations,
                                      double weight, double height, double aveActiveTime, List<Long> preferredExercise, double activeTimeToday) {

        Integer caloriesToBeBurned = recommendExercise(recommendations, weight, height,
                (int) (long) Math.round(aveActiveTime / 1000 / 60), preferredExercise,
                activeTimeToday / 1000 / 60);

        double todayExpandedCalories   = FitDataHandler.fieldCurrentValue(dataReadResults.get(6), Field.FIELD_CALORIES, null);
        double averageExpandedCalories = FitDataHandler.fieldAverage(dataReadResults.get(0), Field.FIELD_CALORIES, null);
        long caloriesConsumedToday = FitDataHandler.caloriesCurrentTotal(dataReadResults.get(2));

        Integer caloriesToBeEaten = caloriesNeededToday(todayExpandedCalories, averageExpandedCalories, weight, height, caloriesToBeBurned, caloriesConsumedToday);
        Log.d(LOG_TAG, "Calories to be eaten today: " + caloriesToBeEaten);


        boolean hasEatenBreakfast = FitDataHandler.hasUserEaten(dataReadResults.get(2), Field.MEAL_TYPE_BREAKFAST);
        boolean hasEatenLunch = FitDataHandler.hasUserEaten(dataReadResults.get(2), Field.MEAL_TYPE_LUNCH);
        boolean hasEatenDinner = FitDataHandler.hasUserEaten(dataReadResults.get(2), Field.MEAL_TYPE_DINNER);

        if(!hasEatenBreakfast && !(hasEatenLunch || hasEatenDinner) ) {
            List<Long> preferredEatingTimes = FitDataHandler.preferredEatingTimes(dataReadResults.get(2), 10, Field.MEAL_TYPE_BREAKFAST);
            Integer forBreakfast = (int) (long) Math.round(caloriesToBeEaten * 0.32);
            caloriesToBeEaten -= forBreakfast;

            recommendEating(recommendations, forBreakfast, preferredEatingTimes, "Breakfast", RecommendationTypes.BREAKFAST);
        }
        if(!hasEatenLunch && !hasEatenDinner ) {
            List<Long> preferredEatingTimes = FitDataHandler.preferredEatingTimes(dataReadResults.get(2), 10, Field.MEAL_TYPE_LUNCH);
            Integer forLunch = (int) (long) Math.round(caloriesToBeEaten * 0.588);
            caloriesToBeEaten -= forLunch;
            recommendEating(recommendations, forLunch, preferredEatingTimes, "Lunch", RecommendationTypes.LUNCH);
        }
        if(!hasEatenDinner ) {
            List<Long> preferredEatingTimes = FitDataHandler.preferredEatingTimes(dataReadResults.get(2), 10, Field.MEAL_TYPE_DINNER);
            recommendEating(recommendations, caloriesToBeEaten, preferredEatingTimes, "Dinner", RecommendationTypes.DINNER);
        }
    }
}
