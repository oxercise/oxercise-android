package uk.co.bossymom.bossymom.NotificationsTools;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


public class NotificationDeviceTimeSet  extends BroadcastReceiver {
    public static String LOG_TAG = "NotificationDeviceTimeSet";

    public void onReceive(Context context, Intent intent) {
        Intent notificationsService = new Intent(context, NotificationTimeChangedService.class);
        Log.d(LOG_TAG, "About to start NotificationTimeChangedService service");
        context.startService(notificationsService);
    }


}
