package uk.co.bossymom.bossymom.CalendarTools;

/**
 * Internal format for events from user calendar.
 * @param ID is the internal Google Calendar ID to identify events.
 * @param beginUTCms and @param endUTCms are beginning and ending times of an event in UTC
 *                   milliseconds since the epoch.
 * @param title is a string of title of an event
 * @param visibility is a boolean flag indicating if the person has selected to view the calendar
 *                   containing the event or has hidden it. (True is visible).
 * @param allDay is a boolean flag indicating if it is an all day event. (True is all day event).
 * @param availablity is a boolean flag indicating if the person has marked that he is busy or the
 *                    events can be scheduled in that time)

 */
public class BossyMomCalendarEvent implements Comparable<BossyMomCalendarEvent> {
    public final int ID;
    public final long beginUTCms, endUTCms;
    public final String title;
    public final boolean visibility, allDay, availability;



    public BossyMomCalendarEvent(int ID, long begin, long end, String title, boolean visibility, boolean allDay, boolean availability){
        this.ID = ID;
        this.beginUTCms = begin;
        this.endUTCms = end;
        this.title = title;
        this.visibility = visibility;
        this.allDay = allDay;
        this.availability = availability;
    }

    // For insertion of events
    public BossyMomCalendarEvent(long begin, long end, String title){
        this(-1, begin, end, title, true, false, false);
    }

    @Override
    public int compareTo(BossyMomCalendarEvent other) {
        return (int) (beginUTCms - other.beginUTCms);
    }
}
