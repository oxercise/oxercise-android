package uk.co.bossymom.bossymom.UITools;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import uk.co.bossymom.bossymom.R;
import uk.co.bossymom.bossymom.RecommendationEngine.RecommendationTypes;

public class EventsListAdapter extends BaseAdapter{

    private static final String LOG_TAG = "EventsListAdapter";
    public static final int QUARTER_HOUR_HEIGHT_DIP = 20;

    private ArrayList<UIEvent> eventsList = new ArrayList<>();
    private Context context;

    public EventsListAdapter(ArrayList<UIEvent> eventsList, Context context) {
        this.eventsList = eventsList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return eventsList.size();
    }

    @Override
    public Object getItem(int position) {
        return eventsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ((UIEvent) getItem(position)).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null)
            convertView = LayoutInflater.from(context).inflate(R.layout.event_item, null);

        Resources resources = context.getResources();

        UIEvent event = (UIEvent) getItem(position);
        RelativeLayout eventWrapper = (RelativeLayout) convertView.findViewById(R.id.event_wrapper);
        RelativeLayout itemWrapper = (RelativeLayout) convertView.findViewById(R.id.item_wrapper);

        ImageView eventIcon = (ImageView) eventWrapper.findViewById(R.id.event_icon);
        TextView eventTitle = (TextView) eventWrapper.findViewById(R.id.event_title);
        TextView eventData = (TextView) eventWrapper.findViewById(R.id.event_data);
        TextView eventDataUnit = (TextView) eventWrapper.findViewById(R.id.event_data_unit);
        TextView eventCountedData = (TextView) eventWrapper.findViewById(R.id.event_counted_data);
        TextView eventTimeInterval = (TextView) eventWrapper.findViewById(R.id.event_time_interval);

        eventTitle.setText(event.title);
        eventData.setText(event.data);
        eventTimeInterval.setText(event.timeInterval());

        setTypeSpecificAttributes(resources, event, eventWrapper,
                eventIcon, eventDataUnit, eventCountedData);

        setEventHeight(event, position, itemWrapper, eventWrapper);

        return convertView;
    }

    private void setEventHeight(UIEvent event, int position, RelativeLayout itemWrapper, RelativeLayout eventWrapper) {
        int eventHeight = durationToHeight(event.duration());
        eventWrapper.setMinimumHeight(eventHeight);

       /* if (position+1 < getCount()) { //There is a next event
            UIEvent nextEvent = eventsList.get(position+1);
            long calendarGap = UIEvent.msToQuarters(nextEvent.startTime - event.endTime);
            int gapHeight = durationToHeight(calendarGap);
            itemWrapper.setPadding(0, 5, 0, gapHeight);
        }*/
    }

    private int durationToHeight(long duration) {
        final float displayScale = context.getResources().getDisplayMetrics().density;
        return (int) (QUARTER_HOUR_HEIGHT_DIP * duration * displayScale + 0.5f);
    }

    @SuppressWarnings("deprecation")
    private void setTypeSpecificAttributes(Resources resources, UIEvent event,
                                           RelativeLayout eventWrapper, ImageView eventIcon,
                                           TextView eventDataUnit, TextView eventCountedData) {
        if (event.type == UIEvent.TYPE_RECOMMENDATION) {
            if (event.recommendationType == RecommendationTypes.EXERCISE) {
                eventIcon.setImageResource(R.drawable.running_icon);
                eventWrapper.setBackgroundColor(resources.getColor(R.color.activityColor));
                eventDataUnit.setText(UIEvent.UNIT_ACTIVITY);
                eventCountedData.setText(UIEvent.DESCRIPTION_ACTIVITY);
                eventCountedData.setVisibility(View.VISIBLE);
            } else if (event.recommendationType == RecommendationTypes.MEAL) {
                eventIcon.setImageResource(R.drawable.food_icon);
                eventWrapper.setBackgroundColor(resources.getColor(R.color.nutritionColor));
                eventDataUnit.setText(UIEvent.UNIT_NUTRITION);
                eventCountedData.setText(UIEvent.DESCRIPTION_NUTRITION);
                eventCountedData.setVisibility(View.VISIBLE);
            } else if (event.recommendationType == RecommendationTypes.SLEEP) {
                eventIcon.setImageResource(R.drawable.clock_icon);
                eventWrapper.setBackgroundColor(resources.getColor(R.color.sleepColor));
                eventDataUnit.setText(UIEvent.UNIT_SLEEP);
                eventCountedData.setText(UIEvent.DESCRIPTION_SLEEP);
                eventCountedData.setVisibility(View.VISIBLE);
            }
        }
        else if (event.type == UIEvent.TYPE_CALENDAR) {
            eventIcon.setImageResource(R.drawable.calendar_icon);
            eventWrapper.setBackgroundColor(resources.getColor(R.color.calendarEventColor));
            eventDataUnit.setText(null);
            eventCountedData.setVisibility(View.GONE);
        } else { Log.e(LOG_TAG, "Unknown UIEvent type."); }
    }
}
