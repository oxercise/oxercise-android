package uk.co.bossymom.bossymom.NotificationsTools;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class NotificationPublisher extends BroadcastReceiver {

    public static String NOTIFICATION_ID = "notification-id";
    public static String NOTIFICATION = "notification";

    public static String LOG_TAG = "Notification Publisher";

    public void onReceive(Context context, Intent intent) {

        Intent notificationsService = new Intent(context, NotificationService.class);

        Notification notification = intent.getParcelableExtra(NOTIFICATION);
        int id = intent.getIntExtra(NOTIFICATION_ID, 0);

        notificationsService.putExtra(NOTIFICATION, notification);
        notificationsService.putExtra(NOTIFICATION_ID, id);

        try {
            Log.d(LOG_TAG, "Started service");
            context.startService(notificationsService);
            //notificationManager.notify(id, notification);
        }
        catch (RuntimeException e){
            Log.e(LOG_TAG, "Null notification");
        };



    }
}