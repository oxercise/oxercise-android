package uk.co.bossymom.bossymom.NotificationsTools;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.Calendar;

import uk.co.bossymom.bossymom.RecommendationEngine.BossyMomRecommendationsEvent;
import uk.co.bossymom.bossymom.RecommendationEngine.RecommendationTypes;

public class NotificationTimeChangedService extends Service {
    public static String NOTIFICATION_ID = "notification-id";
    public static String NOTIFICATION = "notification";
    public static String LOG_TAG = "NotificationTimeChangedService";

    @Override
    public IBinder onBind(Intent arg0)
    {
        return null;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.d(LOG_TAG, "Time has just changed.");
        super.onStartCommand(intent, flags, startId);

        NotificationManager notificationManager = new NotificationManager( this.getApplicationContext() );
        notificationManager.cancelAllNotifications();
        notificationManager.rescheduleAllNotifications();

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

}
