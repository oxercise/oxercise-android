package uk.co.bossymom.bossymom;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import uk.co.bossymom.bossymom.RecommendationEngine.BossyMomRecommendations;
import uk.co.bossymom.bossymom.UITools.EventsListAdapter;
import uk.co.bossymom.bossymom.UITools.UIEvent;

public class MainActivity extends AppCompatActivity {

    private ListView eventsListView;
    private EventsListAdapter listAdapter;
    public ArrayList<UIEvent> eventsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        populateEventsListView();
        setDateCorner();
    }

    private void setDateCorner() {
        Calendar timeNow = Calendar.getInstance();

        Date dateNow = new Date(timeNow.getTimeInMillis());

        TextView todayOfWeek = (TextView) findViewById(R.id.today);
        TextView todayDate = (TextView) findViewById(R.id.today_date);
        todayOfWeek.setText(new SimpleDateFormat("EE").format(dateNow));
        todayDate.setText(timeNow.get(Calendar.DAY_OF_MONTH) + "");
    }

    private void populateEventsListView() {
        Toast.makeText(this, "Computing your recommendations...", Toast.LENGTH_LONG).show();
        new BossyMomRecommendations(this, getContentResolver()).recommend();
    }

    public void setEventsList() {
        eventsListView = (ListView) findViewById(R.id.events_list);
        listAdapter = new EventsListAdapter(eventsList, this);

        eventsListView.setAdapter(listAdapter);
        eventsListView.setDivider(null);

        scanToCurrentEvent();
    }

    private void scanToCurrentEvent() {
        Calendar now = Calendar.getInstance();
        int scrollPosition = 0;
        while (scrollPosition < eventsList.size() &&
                eventsList.get(scrollPosition).endTime < now.getTimeInMillis()) scrollPosition += 1;
        eventsListView.setSelection(scrollPosition);
    }
}
