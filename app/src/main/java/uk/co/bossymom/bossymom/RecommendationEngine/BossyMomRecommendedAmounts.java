package uk.co.bossymom.bossymom.RecommendationEngine;

/**
 * Class for retrieving recommended amounts of calories, sleep and exercise for adults.
 * For now it is just a mock data.
 */
public class BossyMomRecommendedAmounts{

    // For now, let it be in minutes
    public Integer sleep(){
        // 8 hours of sleep per day seems reasonable?
        return 8 * 60;
    }

    // For now, let it be in minutes
    public Integer exercise(){
        // 1.5 hour of exercise per day?
        return 90;
    }

    // For now, let it be in calories
    public Integer intake(){
        // 1500 kcal?
        return 1500;
    }

    public Integer caloriesBurned(Integer time, double weight){
        return (int) (long) Math.round(0.63 * weight  * time / 6.01 / 0.621371);
    }
}
