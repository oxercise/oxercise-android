package uk.co.bossymom.bossymom.UITools;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import uk.co.bossymom.bossymom.RecommendationEngine.RecommendationTypes;

public class UIEvent implements Comparable {

    public static int TYPE_UNKNOWN = 0, TYPE_CALENDAR = 1, TYPE_RECOMMENDATION = 2;
    public static String DESCRIPTION_ACTIVITY = "Energy: ", DESCRIPTION_NUTRITION = "Intake: ", DESCRIPTION_SLEEP = "Duration: ";
    // Sleep duration is calculated in {x h y min} in recommendations, thus giving sleep unit empty.
    public static String UNIT_ACTIVITY = "kCal", UNIT_NUTRITION = "kCal", UNIT_SLEEP = "";

    protected int id = 0;
    protected int type = TYPE_UNKNOWN;
    protected RecommendationTypes recommendationType;
    protected String title = new String();
    protected String data = new String();

    public long startTime, endTime;


    public UIEvent() { }

    public UIEvent(int id, int type, RecommendationTypes recommendationType, long startTime, long endTime,  String title, String data) {
        this.id = id;
        this.type = type;
        this.recommendationType = recommendationType;
        this.title = title;
        this.data = data;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public static long msToQuarters(long msAmount) {
        return msAmount / (1000 * 60 * 15);
    }

    //In quarter of hours
    public long duration() {
        return msToQuarters(endTime - startTime);
    }

    public String timeInterval() {
        Date startDate = new Date(startTime);
        Date endDate = new Date(endTime);

        DateFormat formatter = new SimpleDateFormat("HH:mm");
        return formatter.format(startDate) + " - " + formatter.format(endDate);
    }

    @Override
    public int compareTo(Object another) {
        UIEvent anotherEvent = (UIEvent) another;
        return (int) (startTime - anotherEvent.startTime);
}
}
