package uk.co.bossymom.bossymom.NotificationsTools;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import uk.co.bossymom.bossymom.CalendarTools.BossyMomCalendarEvent;
import uk.co.bossymom.bossymom.MainActivity;
import uk.co.bossymom.bossymom.R;
import uk.co.bossymom.bossymom.NotificationsTools.NotificationPublisher;
import uk.co.bossymom.bossymom.RecommendationEngine.BossyMomRecommendationsEvent;
import uk.co.bossymom.bossymom.RecommendationEngine.RecommendationTypes;

/**
 * Class to show customised notifications
 */
public class NotificationManager {

    // Saving events
    public static final String PREFS_NAME = "BossyMomRecommendedNotificationIDs";
    public static final String EVENTS_IDS_PREF_NAME = "notificationIDs";

    private Context context;
    private String LOG_TAG = "Notifications";
    private long alertBeforeMin = 10;
    private long acceptableNotificationsInThePastMin = 3;
    private AlarmManager alarmManager;


    public NotificationManager(Context context){
        this.context = context;
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }

    public void scheduleRecommendedNotification(BossyMomRecommendationsEvent event){
        scheduleNotification(getNotification(event), event);
        saveNotification(event);
    }

    /** Translate type into a string for saving into shared preferrences
     *
     * @param type Type of the recommendation given (sleep, meal, etc.)
     * @return String containing the unique name for each type
     */
    private String getPrefsName(RecommendationTypes type){
        String prefsID = "previousNotificationsFor";
        switch (type){
            case BREAKFAST: prefsID += "Breakfast"; break;
            case LUNCH:     prefsID += "Lunch"; break;
            case DINNER:    prefsID += "Dinner"; break;
            case EXERCISE:  prefsID += "Exercise"; break;
            case SLEEP:     prefsID += "Sleep"; break;
            case MEAL:      prefsID += "Meal"; break;
        }
        return prefsID;
    }

    /** Store scheduled notification. Stores only the most recent notification of each type.
     * @param event Recommendations event
     */
    public void saveNotification(BossyMomRecommendationsEvent event){
        String pref = getPrefsName(event.extendedType);
        Log.d(LOG_TAG, "Saving " + pref);

        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();

        editor.putLong  (pref + "begin", event.beginUTCms);
        editor.putLong  (pref + "end",   event.endUTCms);
        editor.putString(pref + "title", event.title);
        editor.putString(pref + "data",  event.data);

        editor.commit();
    };

    /**
     * Upon changing the system clock, alarmManager gets confused, thus reset the notifications for
     * saved recommendations (most recent of given type).
     * Do not reschedule the recommendations in the past.
     * @param type
     */
    private void rescheduleNotification(RecommendationTypes type){
        String pref = getPrefsName(type);
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);

        long begin = settings.getLong(pref + "begin", -1);
        long end        = settings.getLong  (pref + "end", -1);
        String title = settings.getString(pref + "title", "");
        String data     = settings.getString(pref + "data", "");

        Calendar now = Calendar.getInstance();
        long timeOfNotificationMs = begin - (alertBeforeMin - acceptableNotificationsInThePastMin) * 60 * 1000;

        if (begin > -1 && timeOfNotificationMs >= now.getTimeInMillis() ) {
            BossyMomRecommendationsEvent notificationEvent = new BossyMomRecommendationsEvent(begin, end, title, type, data);
            scheduleRecommendedNotification(notificationEvent);
        }
    }
    /**
     * Upon changing the system clock, alarmManager gets confused, thus reset the notifications for
     * saved recommendations of each type;
     * Do not reschedule the recommendations in the past.
     */
    public void rescheduleAllNotifications(){
        Log.d(LOG_TAG, "Rescheduling all notifications");
        for(RecommendationTypes type : RecommendationTypes.values())
            rescheduleNotification(type);
    }

    private int nextNotificationID(){
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        int lastID = settings.getInt(EVENTS_IDS_PREF_NAME, 1);

        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(EVENTS_IDS_PREF_NAME, lastID + 1);
        editor.commit();
        return lastID;
    }
    private void resetNotificationsIDs(){
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(EVENTS_IDS_PREF_NAME, 1);
        editor.commit();
    }

    public void cancelAllNotifications(){
        Intent notificationIntent = new Intent(context, NotificationPublisher.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1299123, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        Log.d(LOG_TAG, "Notifications are being cancelled");
        // Cancel alarms
        try {
            alarmManager.cancel(pendingIntent);
        } catch (Exception e) {
            Log.e(LOG_TAG, "AlarmManager update was not canceled. " + e.toString());
        }
        resetNotificationsIDs();
    }

    private void scheduleNotification(Notification notification, BossyMomCalendarEvent event) {
        Intent notificationIntent = new Intent(context, NotificationPublisher.class);

        int nextID = nextNotificationID();
        Log.d(LOG_TAG, "Notification ID: " + nextID);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, nextID);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1299123 + nextID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar now = Calendar.getInstance();
        long futureInMillis = event.beginUTCms - now.getTimeInMillis() + System.currentTimeMillis();
        // Alert before the event
        futureInMillis -= alertBeforeMin * 60 * 1000;

        Log.d(LOG_TAG, "SysClock: " + System.currentTimeMillis() + " event: " + futureInMillis );

        alarmManager.set(AlarmManager.RTC_WAKEUP, futureInMillis, pendingIntent);
        Log.d(LOG_TAG, "Notifications are written");
    }

    /**
     * Builder for a notification object: set an icon depending on the type of event, adds vibration,
     * sets high priority and colour.
     * @param event recommendation event
     * @return notification object
     */
    private Notification getNotification(BossyMomRecommendationsEvent event) {
        Notification.Builder builder = new Notification.Builder(context);
        event.compressType();

        Date beginning = new Date(event.beginUTCms);
        String beginningInWords = new SimpleDateFormat("HH:mm").format(beginning);

        builder.setContentTitle(event.title + " " + beginningInWords);

        switch (event.activityType){
            case EXERCISE:
                builder.setContentText("Duration: " + Math.round((event.endUTCms - event.beginUTCms) / 1000 / 60) + " min");
                builder.setSmallIcon(R.drawable.running_icon);
                builder.setVibrate(new long[]{0, 1000, 500, 600} );
                break;
            case MEAL:
                builder.setContentText("Calories: " + event.data + " kcal");
                builder.setSmallIcon(R.drawable.food_icon);
                builder.setVibrate(new long[]{0, 500, 300, 2000});
                break;
            case SLEEP:
                builder.setContentText("Duration: " + event.data);
                builder.setSmallIcon(R.drawable.clock_icon);
                builder.setVibrate(new long[]{0, 300, 300, 300, 300} );
                break;
            // In case something breaks
            default:
                builder.setSmallIcon(R.drawable.clock_icon);
                long[] vibrationPatter = {0, 1000, 500, 600};
                builder.setVibrate( vibrationPatter );
        }



        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        builder.setSound(uri);

        builder.setPriority(Notification.PRIORITY_HIGH);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setColor(Color.BLUE);
        }

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, MainActivity.class);
        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        builder.setContentIntent(resultPendingIntent);

        return builder.build();
    }
}
