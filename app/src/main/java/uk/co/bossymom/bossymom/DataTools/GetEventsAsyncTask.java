package uk.co.bossymom.bossymom.DataTools;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import uk.co.bossymom.bossymom.CalendarTools.BossyMomCalendar;
import uk.co.bossymom.bossymom.CalendarTools.BossyMomCalendarEvent;
import uk.co.bossymom.bossymom.MainActivity;
import uk.co.bossymom.bossymom.R;
import uk.co.bossymom.bossymom.RecommendationEngine.BossyMomRecommendationsEvent;
import uk.co.bossymom.bossymom.RecommendationEngine.RecommendationTypes;
import uk.co.bossymom.bossymom.UITools.UIEvent;

//EventsList Populator Task to run in parallel with the UI thread
public class GetEventsAsyncTask extends AsyncTask<Void, Void, ArrayList<UIEvent>>{

    MainActivity mainActivity;
    BossyMomCalendar calendar;

    ArrayList<BossyMomRecommendationsEvent> recommendations = new ArrayList<>();
    List<Long> eventIDsToKeep;

    public GetEventsAsyncTask(MainActivity mainActivity, Vector<BossyMomRecommendationsEvent> recommendations, List<Long> eventIDsToKeep) {
        this.mainActivity = mainActivity;
        calendar = new BossyMomCalendar(mainActivity.getContentResolver(), mainActivity);
        this.recommendations.addAll(recommendations);
        this.eventIDsToKeep = eventIDsToKeep;
    }

    @Override
    protected ArrayList<UIEvent> doInBackground(Void... params) {
        ArrayList<UIEvent> uiEvents = new ArrayList<>();

        extractCalendarEvents(uiEvents);
        parseRecommendations(uiEvents);

        Collections.sort(uiEvents);
        //mockData(uiEvents);

        Vector<BossyMomRecommendationsEvent> mbrevector = new Vector<>(recommendations);
        calendar.writeRecommendations(mbrevector);

        
        return uiEvents;
    }

    @Override
    protected void onPostExecute(ArrayList<UIEvent> uiEvents) {
        mainActivity.eventsList.addAll(uiEvents);
        mainActivity.setEventsList();

        final ImageView splashImage = (ImageView) mainActivity.findViewById(R.id.splash_image);
        splashImage.animate()
                .translationY(-splashImage.getHeight())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        splashImage.setVisibility(View.GONE);
                    }
                })
                .setDuration(1000);
    }

    private void parseRecommendations(ArrayList<UIEvent> uiEvents) {
        for (BossyMomRecommendationsEvent recommendation: recommendations) {
            UIEvent uiEvent = new UIEvent(recommendation.ID, UIEvent.TYPE_RECOMMENDATION, recommendation.activityType,
                    recommendation.beginUTCms, recommendation.endUTCms, recommendation.title, recommendation.data);
            uiEvents.add(uiEvent);
        }
    }

    private void extractCalendarEvents(ArrayList<UIEvent> uiEvents) {

        calendar.deleteFutureRecommendations(eventIDsToKeep);

        for (BossyMomCalendarEvent calendarEvent : calendar.getTodaysEvents()) {
            UIEvent uiEvent = new UIEvent(calendarEvent.ID, UIEvent.TYPE_CALENDAR, null,
                    calendarEvent.beginUTCms, calendarEvent.endUTCms, calendarEvent.title, null);
            uiEvents.add(uiEvent);
        }
    }

    private void mockData(ArrayList<UIEvent> uiEvents) {
        uiEvents.add(new UIEvent(0, UIEvent.TYPE_RECOMMENDATION, RecommendationTypes.EXERCISE, 0, 3600000, "Cycle for one hour", "800"));
        uiEvents.add(new UIEvent(1, UIEvent.TYPE_RECOMMENDATION, RecommendationTypes.MEAL, 0, 3600000/4, "Breakfast", "750"));
        uiEvents.add(new UIEvent(2, UIEvent.TYPE_RECOMMENDATION, RecommendationTypes.SLEEP, 0, 3600000*2, "Midday Nap", "2"));
        uiEvents.add(new UIEvent(3, UIEvent.TYPE_CALENDAR, null, 0, 3600000, "Algorithms Lecture", null));
    }
}
