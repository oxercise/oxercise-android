package uk.co.bossymom.bossymom.CalendarTools;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Vector;

import uk.co.bossymom.bossymom.R;
import uk.co.bossymom.bossymom.RecommendationEngine.BossyMomRecommendations;

/**
 * BossyMomCalendarTest -- simple show all calendars/write/read test
 */
public class BossyMomCalendarTest extends AppCompatActivity {
    private String LOGTAG = "BossyMomCalendarTest";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bossy_mom_calendar_test_main);

        final ContentResolver resolver = getContentResolver();
        final BossyMomCalendar c = new BossyMomCalendar(resolver, this);

        Button btn1 = (Button) findViewById(R.id.showCalendarsButton);
        if (btn1 != null) {
            btn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((TextView) findViewById(R.id.statusBar)).setText("Trying to show all calendars");
                    c.getCalendars();
                }

            });
        }

        Button btn2 = (Button) findViewById(R.id.displayEventsButton);
        if (btn2 != null) {
            btn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((TextView) findViewById(R.id.statusBar)).setText("Trying to display events");

                    ArrayList<Long> eventIDs = c.restoreIDs();
                    for(Long ev : eventIDs) {
                        Log.d(LOGTAG, "Saved event id: " + ev);
                    }

                    // Display all today's events in the log, with duration in minutes
                    Vector<BossyMomCalendarEvent> events = c.getTodaysEvents();
                    for (int i = 0; i < events.size(); i++)
                        Log.d(LOGTAG, events.get(i).title + "; duration: " + ((events.get(i).endUTCms - events.get(i).beginUTCms) / 1000 / 60));

                }

            });
        }

        Button btn3 = (Button) findViewById(R.id.addEventButton);
        if (btn3 != null) {
            btn3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((TextView) findViewById(R.id.statusBar)).setText("Trying to insert an event");

                    // Create an event "Full time work" form 03:15 to 08:45
                    Calendar beginTime = Calendar.getInstance();
                    beginTime.set(Calendar.HOUR_OF_DAY, 3);
                    beginTime.set(Calendar.MINUTE, 15);
                    long startMillis = beginTime.getTimeInMillis();

                    Calendar endTime = Calendar.getInstance();
                    endTime.set(Calendar.HOUR_OF_DAY, 8);
                    endTime.set(Calendar.MINUTE, 45);
                    long endMillis = endTime.getTimeInMillis();

                    // Internal representation
                    BossyMomCalendarEvent ev = new BossyMomCalendarEvent(startMillis, endMillis, "Full time work");

                    Log.e(LOGTAG, "Disabled feature.");
                    // Write event to calendar
                    //c.writeEvent(ev);
                    //Log.d(LOGTAG, "Event written to calendar.");
                }

            });
        }

        Button btn4 = (Button) findViewById(R.id.getFreeSlotsButton);
        if (btn4 != null) {
            btn4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((TextView) findViewById(R.id.statusBar)).setText("Showing free slots");

                    Vector<Pair<Calendar, Calendar>> slots = c.getTodaysFreeSlots();

                    for( Pair<Calendar, Calendar> slot : slots)
                        Log.d(LOGTAG, "Free time from " + slot.first.getTime() + " to " + slot.second.getTime());
                }

            });
        }

        final BossyMomRecommendations recommendationsEngine = new BossyMomRecommendations((Context) this, resolver);

        Button btn5 = (Button) findViewById(R.id.getRecommendationsButton);
        if (btn5 != null) {
            btn5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((TextView) findViewById(R.id.statusBar)).setText("Showing recommendations");

                    recommendationsEngine.recommend();
                }

            });
        }

        Button btn6 = (Button) findViewById(R.id.deleteRec);
        if (btn6 != null) {
            btn6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((TextView) findViewById(R.id.statusBar)).setText("Deleting");

                    //c.deleteFutureRecommendations();
                }

            });
        }

    }

}
