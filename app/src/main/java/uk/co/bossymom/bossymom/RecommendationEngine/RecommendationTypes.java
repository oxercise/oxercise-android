package uk.co.bossymom.bossymom.RecommendationEngine;

/**
 * Enum to support different types of recommendations
 */
public enum RecommendationTypes{
    BREAKFAST,
    LUNCH,
    DINNER,
    MEAL,
    EXERCISE,
    SLEEP
}
