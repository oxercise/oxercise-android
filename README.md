# README #

Public repository for the Oxercise Android Application. The app ingests fitness and calendar data and produces recommendations for the user for meals, exercise and sleep. The recommendations are computed to fit in the user’s daily schedule and to optimise his/her well-being.

[Final report available here.](https://drive.google.com/open?id=0B-kc4_C_LZ3hR0hPZG1RTmVrazg)
Well done everybody :)!

# Checking in code #

Please make sure that you are checking in code to your private development branch first. Then, after you merge the latest interface/backend branch into your development branch and thoroughly test your code, ask for a short code review from a teammate before merging into the interface / backend branches. 

Merges of interface / backend into the master branch should NOT be done directly, but into a dummy parallel master-option branch. After the integrated code is fully tested and agreed upon by all team members, merges into the master branch are allowed (we could rely on us just chatting or better, for improved trackability as well, we can use pull requests). 

Soon, the Issues section will be updated with the list of tasks each subteam / team member is currently working one and also with the list of tasks to be done in the future. This should help us have a better idea of where we are in later stages. Please feel free to update the section when you feel the need to raise a new issue, regardless of its type. 

Good luck to us :)!